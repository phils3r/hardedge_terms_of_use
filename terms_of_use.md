# HardEdge Discord Nutzungsbedingungen
## Regeln
1. Im HardEdge Discord Server herrscht grundsätzliche Diskussionsfreiheit. Solange ein Thema in den betreffenden Channel passt, wird es weder gelöscht, verschoben oder bearbeitet.
1. Sollte eine der übrigen HardEdge Discord Regeln durch ein Thema oder einen Beitrag verletzt worden sein, kann diese Regel (Diskussionsfreiheit) außer Kraft treten. Die Sanktionsmöglichkeiten für eine derartige Verletzung liegen ganz im Ermessen der HardEdge Discord Moderatoren.
1. Kein Beitrag darf Menschen aufgrund ihrer ethnischer oder geographischer Herkunft, Hautfarbe und anderer genetischer Merkmale, körperlicher, seelischer oder geistiger Behinderung, sowie ihrer sexuellen oder religiösen Orientierung diskriminieren, oder dazu aufrufen. Auch pornografische, rassistische, gewaltverherlichende oder extremistische Beiträge werden nicht geduldet.
1. Jeder Moderator hat in jedem Fall das Recht, einen Beitrag eines anderen Benutzers zu löschen, wenn er dies Aufgrund des Inhaltes für angemessen empfindet. Was „angemessen“ ist und was nicht, ist Entscheidung des Moderators. Sollte er nach dem Grund der betreffenden Maßnahme gefragt werden, muss er jedoch eine Discord-Regel angeben können, die sein Handeln rechtfertigt.
1. Jedes Mitglied verpflichtet sich durch die Nutzung des HardEdge Discords dazu, mit den anderen Benutzern zivilisiert umzugehen. Dies beinhaltet die gängige Etikette betreffend der Vermeidung von Kraftausdrücken, Beleidigungen und übler Nachrede. Da der Discord eine wichtige Funktion in der Aufrecherhaltung der Beziehungen zwischen Gleichgesinnten hat, ist es wünschenswert, wenn man jedes andere Mitglied so behandeln würde, als ob es ein guter Freund wäre. Im Falle einer Beleidigung kann jeder Benutzer einem HardEdge Moderator über das erzeugen eines Supportickets via Bot @Helpling oder eine Benachrichtung über die Rolle "@mods" auf die Umstände aufmerksam machen.
1. Sollte einmal ein Streit (im Gegensatz zu einer normalen Diskussion) zwischen zwei oder mehreren Benutzern zustande kommen, so ist dieser außerhalb des öffentlichen Bereichs des Discords auszutragen. Es liegt im Zweifelsfall im Ermessen der HardEdge - Teammitlieder, was über eine normale Diskussion hinausgeht und bereits ein Streit ist. Um Streitigkeiten „extern“ zu beenden, sollen sich die Parteien via Discord-DMs o.ä. verständigen.
1. Verleumdungen bzw. Anschwärzungen eines Users oder Teammitgliedes sind in einem Discord nicht gerne gesehen und führen sehr häufig zum Streit. Deshalb ist dies nach Möglichkeit zu unterlassen. Davon sind allerdings Hinweise zum Fehlverhalten eines Users ausgeschlossen. Solche Hinweise sind beim Team immer willkommen und notwendig. Deshalb bedankt sich das Team herzlich im Voraus, für jeden solchen Hinweis.
1. Beschwerden über das Verhalten einer oder mehrerer User müssen Moderatoren durch die Rolle "@mods" oder durch das eröffnen eines Falles im Ticketsystem durch den Botbenutzer @Helpling benachrichtigt werden.
1. Missbrauch des Bots @Helpling wird hart bestraft
1. Das unerlaubte Veröffentlichen von privaten Chatmitschnitten ist untersagt. Missstände, Probleme und ähnliches ist mit der Moderation zu klären.
1. Das Veröffentlichen von Einzelbildern, welche explizit einzelne Mitglieder des Discords zeigen, ist ohne Erlaubnis zu unterlassen. Davon ausgenommen sind Berichterstattung und Bilderalben von offline Turnieren. Generell gilt die Beachtung der Privatssphäre der einzelnen Person.
1. Konstruktive Kritik zum Discord muss im Feedback Channel abgegeben werden.
1. Beiträge welche nicht direkt mit dem Channel Topic zu tun haben, sind solange geduldet, sofern sie den Channelfluss nicht stören oder anderweitig negativ beeinflußen.
1. Für Inhalte auf anderen öffentlichen Plattformen wird nicht gehaftet. Allerdings behält sich das Moderationsteam vor, bei bestimmten Inhalten (Extremismus, Nazi-Symbolik, etc), auch aufgrund von diesen, Sanktionen zu verhängen oder gar Nutzer auszuschließen.
1. Sinnlose und kontinuierliche negative Aussagen über Spiele sind zu unterlassen. Dies ist vor allem dann einzuhalten, wenn sich andere User im Channel hiervon belästigt fühlen.
1. Provokative und Schimpfwort-ähnliche Usernamen sind zu unterlassen. User sollen nicht exzessiv ihren Nutzernamen für den HardEdge Discord ändern. Unnötig lange Usernamen, sowie reine Emoji-basierte und übertriebene Emoji Nutzung beim Nutzernamen sind auch zu unterlassen.

### aktuell nicht gültig und diskussionswürdig
* Offensichtlich nach deutschem Recht stark rechtswidrige Inhalte sind mit besonder Sorgfalt und Vertraulichkeit zu behandeln.


# HardEdge Discord terms of use
## Rules
1. General freedom of opinion applies to the hardedge discord server. As long as a topic is in relation to the channel, it will not be deleted, moved or edited.
1. Should any other hardedge discord rule be infringed by a certain topic or a post, said rule (freedom of opinion) will not apply. Sanction options for such infrigements completely oblige the rating of the HardEdge discord moderators.
1. No post may discriminate against people on the basis of their ethnic or geographical origin, skin colour and other genetic characteristics, physical, mental or spiritual handicap, or their sexual or religious orientation, or call for such discrimination. Also pornographic, racist, violence glorifying or extremist contributions are not tolerated.
1. In each case, every moderator has the right to delete the post of another user, if he deems its content inappropriate. What counts as "appropriate" or not is left to his decision. Should he be asked about the reason for this action, he does have to refer to a Discord-rule, that justifies his action.
1. By using the HardEdge Discord each member commits himself to treat the other users in a civilized manner. This includes the usual etiquette regarding the avoidance of strong language, insults and defamation. Since the discord has an important function in maintaining relations between like-minded people, it is desirable to treat every other member as if he or she were a good friend. In case of an insult, any user can alert a HardEdge moderator to the circumstances by creating a support ticket via bot @Helpling  or a notification via the @mods role.
1. In case of dispute (unlike a regular discussion) between two or more users, the conversation has to be moved outside of the public area of the Discord. In case of doubt, the HardEdge-team-member will have to decide where a regular discussion ends and where a dispute begins. In order to handle disputes "externally" the parties have to communicate via Discord-DMs or similar ways.
1. Defamation towards a user or a team-member are not welcomed in the discord and often lead to dispute. As such this is to be avoided. This does not refer to hinting towards actual misconduct of other members. The team always welcomes such hints and they are necessary. Therefore, the team is grateful for any such hint.
1. Complaints about the behaviour of one or more users must be notified to moderators by the role "@mods" or by opening a support case via the bot @Helpling.
1. Abuse of the bot @Helpling will be severely punished
1. The publication of single images, which explicitly show single members of HardEdge or this discord, is prohibited without permission. Excluded from this are reports and picture albums of offline tournaments. In general, the privacy of the individual person is respected.
1. Constructive feedback regarding the discord may be stated in the Feedback channel.
1. Posts that do not relate to the topic of the channel are allowed, as long as they do not disrupt the flow of the channel or provide other negative influence.
1. No liability is assumed for content on other public platforms. However, the moderation team reserves the right to impose sanctions for certain content (extremism, Nazi symbolism, etc.), also due to these, or even to exclude users.
1. Pointless and continious negative statements about games are to be refrained from. This is especially the case, if other users in the channel feel bothered by them.
1. Provocative and insult-like usernames are to be refrained from. Users should not excessively change their username for the HE-Discord. Unnecessairily long, as well as purely emoji-based usernames or exaggerated use of emojis within the username should be avoided, too.


### to be discussed
* Obviously according to German law strongly illegal contents are to be treated with special care and confidentiality.
